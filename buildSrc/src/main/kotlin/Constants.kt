object DependenciesHolder {
    const val appcompat = "com.android.support:appcompat-v7:${VersionsHolder.supportLibrary}"
    const val cardView = "com.android.support:cardview-v7:${VersionsHolder.supportLibrary}"
    const val constraintLayout = "com.android.support.constraint:constraint-layout:${VersionsHolder.constraintLayout}"
    const val customTabs = "com.android.support:customtabs:${VersionsHolder.supportLibrary}"
    const val design = "com.android.support:design:${VersionsHolder.supportLibrary}"
    const val glide = "com.github.bumptech.glide:glide:${VersionsHolder.glide}"
    const val glideAnnotationProcessor = "com.github.bumptech.glide:compiler:${VersionsHolder.glide}"
    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${VersionsHolder.kotlin}"
    const val moshi = "com.squareup.moshi:moshi:${VersionsHolder.moshi}"
    const val moshiCodeGen = "com.squareup.moshi:moshi-kotlin-codegen:${VersionsHolder.moshi}"
    const val recyclerView = "com.android.support:recyclerview-v7:${VersionsHolder.supportLibrary}"
    const val retrofit = "com.squareup.retrofit2:retrofit:${VersionsHolder.retrofit}"
    const val retrofitMoshi = "com.squareup.retrofit2:converter-moshi:${VersionsHolder.retrofit}"
}

object PluginsHolder {
    const val android = "com.android.tools.build:gradle:${VersionsHolder.android}"
    const val kotlin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${VersionsHolder.kotlin}"
}

object VersionsHolder {
    const val android = "3.1.4"
    const val compileSdk = 28
    const val constraintLayout = "1.1.3"
    const val glide = "4.8.0"
    const val kotlin = "1.2.70"
    const val minSdk = 16
    const val moshi = "1.6.0"
    const val retrofit = "2.4.0"
    const val supportLibrary = "28.0.0-rc02"
    const val targetSdk = compileSdk
}