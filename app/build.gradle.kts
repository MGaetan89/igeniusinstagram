plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")
}

android {
    compileSdkVersion(VersionsHolder.compileSdk)

    defaultConfig {
        applicationId = "ia.igenius.instagram"
        minSdkVersion(VersionsHolder.minSdk)
        targetSdkVersion(VersionsHolder.targetSdk)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false

            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    sourceSets {
        getByName("androidTest").java.srcDirs("src/androidTest/kotlin")
        getByName("main").java.srcDirs("src/main/kotlin")
        getByName("test").java.srcDirs("src/test/kotlin")
    }
}

dependencies {
    kapt(DependenciesHolder.glideAnnotationProcessor)
    kapt(DependenciesHolder.moshiCodeGen)

    implementation(DependenciesHolder.appcompat)
    implementation(DependenciesHolder.cardView)
    implementation(DependenciesHolder.constraintLayout)
    implementation(DependenciesHolder.customTabs)
    implementation(DependenciesHolder.design)
    implementation(DependenciesHolder.glide)
    implementation(DependenciesHolder.kotlin)
    implementation(DependenciesHolder.moshi)
    implementation(DependenciesHolder.recyclerView)
    implementation(DependenciesHolder.retrofit)
    implementation(DependenciesHolder.retrofitMoshi)
}
