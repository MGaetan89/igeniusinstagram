package ia.igenius.instagram.adapter.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import ia.igenius.instagram.R
import ia.igenius.instagram.extension.inflate
import ia.igenius.instagram.model.Media
import ia.igenius.instagram.view.MediaStatsView

class MediaViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val image = view.findViewById<ImageView>(R.id.image)
    private val stats = view.findViewById<MediaStatsView>(R.id.stats)

    init {
        view.setOnClickListener {
            // TODO
        }
    }

    fun bindTo(media: Media) {
        media.images?.standardResolution?.url?.let {
            Glide.with(image)
                .load(it)
                .into(image)
        }

        stats.setMedia(media)
    }

    companion object {
        fun withParent(parent: ViewGroup): MediaViewHolder {
            val view = parent.inflate(R.layout.adapter_media)

            return MediaViewHolder(view)
        }
    }
}
