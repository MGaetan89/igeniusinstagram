package ia.igenius.instagram.adapter

import android.support.v7.recyclerview.extensions.AsyncListDiffer
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import ia.igenius.instagram.adapter.viewholder.MediaViewHolder
import ia.igenius.instagram.model.Media

class MediaAdapter : RecyclerView.Adapter<MediaViewHolder>() {
    private val differ = AsyncListDiffer<Media>(this, DiffCallback)

    override fun getItemCount() = this.differ.currentList.size

    override fun onBindViewHolder(holder: MediaViewHolder, position: Int) {
        holder.bindTo(this.differ.currentList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MediaViewHolder {
        return MediaViewHolder.withParent(parent)
    }

    fun submitList(media: List<Media>) {
        this.differ.submitList(media)
    }

    private object DiffCallback : DiffUtil.ItemCallback<Media>() {
        override fun areContentsTheSame(oldItem: Media, newItem: Media) = oldItem == newItem

        override fun areItemsTheSame(oldItem: Media, newItem: Media) = oldItem.id == newItem.id
    }
}
