package ia.igenius.instagram

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import ia.igenius.instagram.extension.accessToken
import ia.igenius.instagram.extension.create
import ia.igenius.instagram.fragment.LoginFragment
import ia.igenius.instagram.fragment.MediaFeedFragment
import ia.igenius.instagram.model.AccessToken
import ia.igenius.instagram.network.InstagramApi
import kotlinx.android.synthetic.main.activity_main.toolbar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        handleIntent(intent)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)

        handleIntent(intent)
    }

    private fun displayMediaFeed() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.content, MediaFeedFragment.newInstance())
            .commit()
    }

    private fun displayLogin() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.content, LoginFragment.newInstance())
            .commit()
    }

    private fun handleIntent(intent: Intent) {
        when (intent.action) {
            Intent.ACTION_MAIN -> {
                val accessToken = PreferenceManager.getDefaultSharedPreferences(this).accessToken
                if (accessToken.isEmpty()) {
                    displayLogin()
                } else {
                    displayMediaFeed()
                }
            }
            Intent.ACTION_VIEW -> {
                intent.data?.getQueryParameter("code")?.let(this::requestAccessToken)
            }
        }
    }

    private fun requestAccessToken(code: String) {
        val baseUrl = getString(R.string.instagram_base_url)
        val clientId = getString(R.string.instagram_client_id)
        val clientSecret = getString(R.string.instagram_client_secret)
        val redirectUri = getString(R.string.instagram_redirect_uri)

        Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
            .create<InstagramApi>()
            .requestAccessToken(clientId, clientSecret, "authorization_code", redirectUri, code)
            .enqueue(object : Callback<AccessToken> {
                override fun onFailure(call: Call<AccessToken>, t: Throwable) = Unit

                override fun onResponse(call: Call<AccessToken>, response: Response<AccessToken>) {
                    if (response.isSuccessful) {
                        response.body()?.accessToken?.let { accessToken ->
                            val preferences = PreferenceManager.getDefaultSharedPreferences(this@MainActivity)
                            preferences.accessToken = accessToken

                            displayMediaFeed()
                        }
                    }
                }
            })
    }
}
