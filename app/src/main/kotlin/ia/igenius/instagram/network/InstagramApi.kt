package ia.igenius.instagram.network

import ia.igenius.instagram.model.AccessToken
import ia.igenius.instagram.model.Media
import ia.igenius.instagram.model.ResponseWrapper
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface InstagramApi {
    @GET("v1/users/self/media/recent")
    fun getUserMedia(@Query("access_token") accessToken: String): Call<ResponseWrapper<List<Media>>>

    @FormUrlEncoded
    @POST("oauth/access_token")
    fun requestAccessToken(
        @Field("client_id") clientId: String,
        @Field("client_secret") clientSecret: String,
        @Field("grant_type") grantType: String,
        @Field("redirect_uri") redirectUrl: String,
        @Field("code") code: String
    ): Call<AccessToken>
}
