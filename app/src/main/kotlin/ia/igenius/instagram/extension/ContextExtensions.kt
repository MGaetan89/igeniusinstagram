package ia.igenius.instagram.extension

import android.content.Context
import android.support.annotation.ColorRes
import android.support.v4.content.ContextCompat

fun Context.getColorCompat(@ColorRes id: Int) = ContextCompat.getColor(this, id)
