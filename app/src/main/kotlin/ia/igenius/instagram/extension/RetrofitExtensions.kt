package ia.igenius.instagram.extension

import retrofit2.Retrofit

inline fun <reified T : Any> Retrofit.create(): T = this.create(T::class.java)
