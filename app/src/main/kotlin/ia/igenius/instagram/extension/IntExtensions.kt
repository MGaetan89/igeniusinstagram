package ia.igenius.instagram.extension

import java.text.NumberFormat

fun Int.toFormattedValue(): String = NumberFormat.getIntegerInstance().format(this)
