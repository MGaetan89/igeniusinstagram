package ia.igenius.instagram.extension

import android.content.SharedPreferences

var SharedPreferences.accessToken: String
    get() = this.getString("access_token", "").orEmpty()
    set(value) = this.edit().putString("access_token", value).apply()
