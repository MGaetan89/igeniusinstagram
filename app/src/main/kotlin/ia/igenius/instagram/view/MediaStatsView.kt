package ia.igenius.instagram.view

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import ia.igenius.instagram.R
import ia.igenius.instagram.extension.inflate
import ia.igenius.instagram.extension.toFormattedValue
import ia.igenius.instagram.model.Media
import kotlinx.android.synthetic.main.view_media_stats.view.comments
import kotlinx.android.synthetic.main.view_media_stats.view.likes

class MediaStatsView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {
    init {
        this.inflate(R.layout.view_media_stats, true)
    }

    fun setMedia(media: Media) {
        comments.text = media.comments.count.toFormattedValue()
        likes.text = media.likes.count.toFormattedValue()
    }
}
