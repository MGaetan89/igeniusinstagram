package ia.igenius.instagram.fragment

import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ia.igenius.instagram.R
import ia.igenius.instagram.adapter.MediaAdapter
import ia.igenius.instagram.extension.accessToken
import ia.igenius.instagram.extension.create
import ia.igenius.instagram.model.Media
import ia.igenius.instagram.model.ResponseWrapper
import ia.igenius.instagram.network.InstagramApi
import kotlinx.android.synthetic.main.fragment_media_feed.list
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class MediaFeedFragment : Fragment(), Callback<ResponseWrapper<List<Media>>> {
    private val adapter = MediaAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val accessToken = PreferenceManager.getDefaultSharedPreferences(requireContext()).accessToken
        val baseUrl = getString(R.string.instagram_base_url)

        Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
            .create<InstagramApi>()
            .getUserMedia(accessToken)
            .enqueue(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_media_feed, container, false)
    }

    override fun onFailure(call: Call<ResponseWrapper<List<Media>>>, t: Throwable) = Unit

    override fun onResponse(call: Call<ResponseWrapper<List<Media>>>, response: Response<ResponseWrapper<List<Media>>>) {
        if (response.isSuccessful) {
            response.body()?.data?.let(adapter::submitList)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val columnsCount = this.resources.getInteger(R.integer.columns_count)

        list.adapter = adapter
        list.layoutManager = StaggeredGridLayoutManager(columnsCount, StaggeredGridLayoutManager.VERTICAL)
    }

    companion object {
        fun newInstance() = MediaFeedFragment()
    }
}
