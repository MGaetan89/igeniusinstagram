package ia.igenius.instagram.fragment

import android.content.ComponentName
import android.net.Uri
import android.os.Bundle
import android.support.customtabs.CustomTabsClient
import android.support.customtabs.CustomTabsIntent
import android.support.customtabs.CustomTabsServiceConnection
import android.support.v4.app.Fragment
import ia.igenius.instagram.R
import ia.igenius.instagram.extension.getColorCompat

class LoginFragment : Fragment() {
    private val serviceConnection = object : CustomTabsServiceConnection() {
        override fun onCustomTabsServiceConnected(name: ComponentName, client: CustomTabsClient) {
            val context = requireContext()
            val baseUrl = getString(R.string.instagram_base_url)
            val clientId = getString(R.string.instagram_client_id)
            val redirectUri = getString(R.string.instagram_redirect_uri)
            val url = getString(R.string.instagram_authorization_url, baseUrl, clientId, redirectUri)

            CustomTabsIntent.Builder()
                .enableUrlBarHiding()
                .setShowTitle(true)
                .setToolbarColor(context.getColorCompat(R.color.primary))
                .build()
                .launchUrl(context, Uri.parse(url))
        }

        override fun onServiceDisconnected(name: ComponentName) = Unit
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        context?.let {
            CustomTabsClient.bindCustomTabsService(it, "com.android.chrome", serviceConnection)
        }
    }

    override fun onDestroy() {
        context?.unbindService(serviceConnection)

        super.onDestroy()
    }

    companion object {
        fun newInstance() = LoginFragment()
    }
}
