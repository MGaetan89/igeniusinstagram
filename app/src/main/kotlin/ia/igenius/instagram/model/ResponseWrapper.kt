package ia.igenius.instagram.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

open class ResponseWrapper<T>(
    val data: T,
    val meta: Meta,
    val pagination: Pagination
) {
    @JsonClass(generateAdapter = true)
    class Meta(
        val code: Int,
        @Json(name = "error_message") val errorMessage: String?,
        @Json(name = "error_type") val errorType: String?
    )

    @JsonClass(generateAdapter = true)
    class Pagination(
        @Json(name = "next_max_id") val nextMaxId: Int?,
        @Json(name = "next_url") val nextUrl: String?
    )
}
