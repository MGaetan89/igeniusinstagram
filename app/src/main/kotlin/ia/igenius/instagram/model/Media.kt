package ia.igenius.instagram.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Media(
    val caption: Caption?,
    val comments: Count,
    @Json(name = "created_time") val createdTime: String,
    val filter: String,
    val id: String,
    val images: ImageResolution?,
    val likes: Count,
    val link: String,
    val location: Location?,
    val tags: List<String>,
    val type: String, // carousel, image, video
    val user: User,
    @Json(name = "users_in_photo") val usersInPhoto: Array<User>?,
    val videos: Resolution?
) {
    @JsonClass(generateAdapter = true)
    class Caption(
        @Json(name = "created_time") val createdTime: String,
        val from: User,
        val id: String,
        val text: String
    )

    class Count(val count: Int)

    @JsonClass(generateAdapter = true)
    class ImageResolution(
        val thumbnail: Data,
        @Json(name = "low_resolution") lowResolution: Data,
        @Json(name = "standard_resolution") standardResolution: Data
    ) : Resolution(lowResolution, standardResolution)

    @JsonClass(generateAdapter = true)
    class Location(
        val id: String,
        val latitude: Float,
        val longitude: Float,
        val name: String,
        @Json(name = "street_address") val streetAddress: String?
    )

    @JsonClass(generateAdapter = true)
    open class Resolution(
        @Json(name = "low_resolution") val lowResolution: Data,
        @Json(name = "standard_resolution") val standardResolution: Data
    ) {
        class Data(
            val height: Int,
            val url: String,
            val width: Int
        )
    }
}
