package ia.igenius.instagram.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class User(
    @Json(name = "full_name") val fullName: String,
    val id: String,
    @Json(name = "profile_picture") val profilePicture: String,
    val username: String
)
