package ia.igenius.instagram.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class AccessToken(
    @Json(name = "access_token") val accessToken: String,
    val user: User
)
